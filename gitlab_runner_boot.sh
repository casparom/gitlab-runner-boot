#!/bin/sh

DOCKER_SOCKET=/var/run/docker.sock
DEFAULT_CONFIG_DIR=~

if ! [ -x "$(command -v uname)" ]; then
	DOCKER_SOCKET=//var/run/docker.sock
	DEFAULT_CONFIG_DIR=/c/Users
fi

TOKEN=$1
EXECUTOR=$2
EXECUTOR_IMAGE=$3
DATA_DIR=${4-$DEFAULT_CONFIG_DIR/gitlab-runner/$TOKEN/config}
COUNT=${5-1}
CONTAINER_NAME=${6-gitlab-runner-$TOKEN}

IMG=gitlab/gitlab-runner:latest
CONFIG_VOLUME=$DATA_DIR:/etc/gitlab-runner
DOCKER_SOCKET_VOLUME=$DOCKER_SOCKET:/var/run/docker.sock

echo Data dir: $DATA_DIR
install () {
	echo "Installing gitlab-runner service"
	docker run -d --name $CONTAINER_NAME --restart always -v $CONFIG_VOLUME -v $DOCKER_SOCKET_VOLUME $IMG
	echo "Gitlab-runner service installed"
}

register () {
	echo "Registering $COUNT runners"
	for c in $(seq 1 $COUNT)
	do
		NAME="runner_$c"
		docker run --rm -v $CONFIG_VOLUME $IMG register -n -u https://gitlab.com/ --name $NAME -r $TOKEN --executor $EXECUTOR --docker-image $EXECUTOR_IMAGE --docker-volumes /var/run/docker.sock:/var/run/docker.sock
	done
}

install && \
register

exit $?