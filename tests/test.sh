#!/bin/sh

IMG=gitlab/gitlab-runner:latest
DATA_DIR=/tmp/config
CONFIG_VOLUME=$DATA_DIR:/etc/gitlab-runner
COUNT=3
TOKEN=4VFBq9hHMUzEBfkNJNM8
CONTAINER="test-gitlab-runner-$TOKEN"

run_test() {
	sh "$PWD/gitlab_runner_boot.sh" $TOKEN shell alpine $DATA_DIR $COUNT $CONTAINER
}

verify() {
	ACTUAL_COUNT=$(docker run -t --rm -v $CONFIG_VOLUME gitlab/gitlab-runner verify | grep -ic "Verifying runner... is alive")
	echo $((COUNT-$ACTUAL_COUNT))
}

rollback() {
	echo "Starting rollback"
	docker run --rm -v $CONFIG_VOLUME $IMG unregister --all-runners
	docker run --rm -v $CONFIG_VOLUME $IMG stop
	docker run --rm -v $CONFIG_VOLUME $IMG uninstall
	docker rm -f $CONTAINER
	echo "Rollback finished"
}

echo "Starting the test"
run_test
RESULT=$?

if [ $RESULT -eq 0 ]; then
	RESULT=$(verify)
fi

rollback

if [ $RESULT -eq 0 ]
then
	echo Success
else
	echo Failure: $RESULT
fi
exit $RESULT