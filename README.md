# Gitlab-runner-boot

## Prerequisites:
1. Docker must be installed and in path

## Usage:
1. download the script
3. run a shell
4. navigate the the directory with the script
5. run the script

## Example:
---
    ./gitlab_runner_boot.sh myProjectToken docker alpine:latest /c/Path/to/persist/config 3 my_gitlab_runners
where

* myProjectToken is the project token from gitlab
* alpine:latest is the docker image to use in the docker executors of the runners
* /c/Path/to/persist/config is the directory in which to persist the service configuration
* 3 is the number of runners to register
* my_gitlab_runners is the name that will be given to the docker container where the gitlab-runner service runs

###### author:Caspar Romot